#
# Event Type: 49142020
#
# ASCII decay Descriptor: phi -> mu+ mu-
#
from Configurables import Generation
Generation().EventType = 49142020
Generation().SampleGenerationTool = "Special"
from Configurables import Special
Generation().addTool( Special )
Generation().Special.ProductionTool = "PythiaProduction"
from Configurables import ToolSvc
#from Configurables import EvtGenDecay
#ToolSvc().addTool( EvtGenDecay )
#ToolSvc().EvtGenDecay.UserDecayFile = "cep_genex_phi_mumu.dec"
Generation().Special.CutTool = ""
Generation().FullGenEventCutTool = "LoKi::FullGenEventCut/cepInAcc"

# Configure the event type.
from Configurables import (Generation, Special, GenExProduction)
from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import importOptions

# Generation options.
Generation().PileUpTool           = "FixedLuminosityForRareProcess"
Generation().DecayTool            = ""
Generation().SampleGenerationTool = "Special"

# Special options.
Generation().addTool(Special)
Generation().Special.CutTool        = ""
Generation().Special.DecayTool      = ""
Generation().Special.ProductionTool = "GenExProduction"

# Stop pile-up generation.
Generation().PileUpTool = "FixedNInteractions"

# GenEx options.
Generation().Special.addTool(GenExProduction)
Generation().Special.GenExProduction.process_mesonId = 333
#Generation().Special.GenExProduction.model = "KtFactorization"
Generation().Special.GenExProduction.kinematics_minPt = 0.
Generation().Special.GenExProduction.kinematics_maxPt = 0.8
Generation().Special.GenExProduction.kinematics_minRapidity = 2.0
Generation().Special.GenExProduction.kinematics_maxRapidity = 5.0
Generation().Special.GenExProduction.internal_generateNTuple = 1
# Cut on the J/psi.
from Configurables import LoKi__FullGenEventCut
Generation().addTool(LoKi__FullGenEventCut, "cepInAcc")
cepInAcc = Generation().cepInAcc
cepInAcc.Code = "( count( out1 ) == 1 )"
cepInAcc.Preambulo += [
    "from GaudiKernel.SystemOfUnits import GeV, mrad",
    "out1 = ( ( GABSID == 333 ) & ( GETA > 1.0 ) & ( GETA < 6.0 ) )"]

# Keep the CEP process in MCParticles.
from Configurables import GenerationToSimulation
GenerationToSimulation("GenToSim").KeepCode = ("( GBARCODE >= 2 )")

