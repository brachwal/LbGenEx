#ifndef GENEXPROCESSBASE_H
#define GENEXPROCESSBASE_H

//standard libraries
#include <vector>
#include <utility>
#include <iostream>

//local
#include "GenExRndmWrapper.h"
#include "GenExSettings.h"
#include "GenExParticleInfo.h"
//Gaudi (XYZVector)
#include "Generators/IBeamTool.h"

//ROOT
#include "TLorentzVector.h"

//Foam
#include "TFoamIntegrand.h"

//GenEx
#include "TEventMaker2toN.h"

namespace GenEx{


/**	@class GenExProcessBase GenExProcessBase.h GenEx/processes/GenExProcessBase.h
*
*	Class serving as a base for defining processes.
*
*	@author Mateusz Goncerz
*	@date 2017-07-15
*
*/
class GenExProcessBase : public TFoamIntegrand {

public:

	
	///default constructor is disabled
	GenExProcessBase() = delete;
	
	///virtual destructor to make it a base class
	~GenExProcessBase() = 0;

	/**	
	*
	*	A constructor that sets the references to particle in/out vectors
	*	and 
	*
	*/
	GenExProcessBase(
			GenExRndmWrapper& randGen,
			GenExParticleInfo& generatorParticles_,
			GenExSettings::Kinematics const& settingsKinematics)
		: m_randGen(randGen)
		, generatorParticles(generatorParticles_)
		, m_eventMaker(generatorParticles.m_CMEnergy,
				settingsKinematics.cuts.m_minPt,
				settingsKinematics.cuts.m_maxPt,
				settingsKinematics.cuts.m_minRapidity,
				settingsKinematics.cuts.m_maxRapidity, 
		  		generatorParticles.m_outParticles.first.size() - 1,
				settingsKinematics.internal.m_minBlobMass,
				settingsKinematics.internal.m_maxBlobMass, 
				generatorParticles.m_outParticlesMasses.data(),
		  		generatorParticles.m_inParticles.first.data(), 
				generatorParticles.m_outParticles.first.data(),
				generatorParticles.m_inParticles.second.data(),
		  		generatorParticles.m_outParticles.second.data(), 
				settingsKinematics.m_isol)
		{
			std::cout<< "\t[DEBUG]:: GenExProcessBase defining!" <<std::endl;
			 m_randGen.initializeDistribution(); //first initialization, then it's initialized inside density function
			std::cout<< "\t[DEBUG]:: GenExProcessBase defined!" <<std::endl;

		}

	/**	
	*	Density function that Foam uses for integration 
	*	and phase space creation. 
	*
	*	It has to return the weight that Foam uses during the initialization
	*	to set up random number distribution. For details, please consult Foam documentation.
	*
	*	First argument is a number of dimensions, while the second
	*	is an array of random numbers [0,1] from the distribution set by returned weight.
	*/
	Double_t Density(Int_t, Double_t*);

	/**
	*	Returns the square of pp->pVp amplitude
	*	can be overriden by processes, defaults to 1
	*	use particle pointers inside
	*/
	virtual Double_t calculateSquaredAmplitude();
	
	
protected:

		bool applyGeometryConstraints() const;

		GenExRndmWrapper& m_randGen;  
		///particles
		GenExParticleInfo& generatorParticles;

		///phase space creator
		TEventMaker2toN m_eventMaker; //works in GeV
		

};
}

#endif
