#ifndef GENEXSETTINGS_H
#define GENEXSETTINGS_H

#include "TRandom3.h" //Double_t and Int_t
namespace GenEx{


/**	@class Settings GenExGenerator.h GenEx/GenExGenerator.h
*
*	A class containing all of the available settings.
*	It is shared with the interface via a pointer
*	and the former is required to fill and provide it.
*	Settings in Internal structs are, with an exception of 
*	beamtoolName, overriden on generator level.
*/
class GenExSettings final{
	
public:

	struct Amplitude{
		bool m_abs_corr; ///amplitude absorption corrections
	} amplitude;

	struct Foam{
		///Foam settings (consult Foam documentation)
		Long_t m_Cells;
		Long_t m_nSampl;
		Int_t m_nBin;
		Int_t m_OptRej;
		Int_t m_OptDrive;
		Int_t m_EvPerBin;
		Int_t m_Chat;
		Double_t m_MaxWtRej;
	} foam;

	struct Internal{
		bool m_generateNTuple;
		std::string m_nTupleName;

		///beam tool
		std::string m_beamtoolName;
	} internal;

	struct Process{
		///generation settings
		Int_t m_mesonID;
		std::string m_decayProductsID;

	} process;

	struct Kinematics{
		Int_t m_isol; ///from TEventMaker2toN
		Double_t m_mesonMassWidth;

		struct Cuts{
			///phase space cuts applied in TEventMaker2toN class
			Double_t m_minPt;
			Double_t m_maxPt;
			Double_t m_minRapidity;
			Double_t m_maxRapidity;
		} cuts;
		
		struct Internal{
			Double_t m_minBlobMass;
			Double_t m_maxBlobMass;
		} internal;
	} kinematics;
	



	/**
	*	Performs a basic sanity check on settings
	*	in order to avoid crashes and easy to miss errors
	*/
	inline bool basicSttgsCheck() const{

		bool isGood = 1;
	

		if(foam.m_Cells <= 1) {std::cout << "GenEx::Foam::Number of cells less than or equal to 1"<< std::endl; isGood = 0;}
		if(foam.m_nSampl <= 1) {std::cout << "GenEx::Foam::Number of samples less than or equal to 1"<< std::endl; isGood = 0;}
		if(foam.m_nBin <= 1) {std::cout << "GenEx::Foam::Number of bins less than or equal to 1"<< std::endl; isGood = 0;}
		if(foam.m_OptRej != 0 && foam.m_OptRej != 1) {std::cout << "GenEx::Foam::Wrong value of OptRej"<< std::endl; isGood = 0;}
		if(foam.m_OptDrive <= 0) {std::cout << "GenEx::Foam::OptDrive less than or equal to 0"<< std::endl; isGood = 0;}
		if(foam.m_EvPerBin <= 1) {std::cout << "GenEx::Foam::Number of events per bin less than or equal to 1"<< std::endl; isGood = 0;}
		if(foam.m_Chat != 0 && foam.m_Chat != 1 && foam.m_Chat != 2) {std::cout << "GenEx::Foam::Desired chat level doesn't exist"<< std::endl; isGood = 0;}
		if(foam.m_MaxWtRej <= 0) {std::cout << "GenEx::Foam::MaxWtRej less than or equal to 0"<< std::endl; isGood = 0;}
	
		return isGood;
	}

};



}



#endif