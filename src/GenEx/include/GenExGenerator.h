#ifndef GENEXGENERATOR_H
#define GENEXGENERATOR_H

//standard libraries
#include <string>
#include <vector>
#include <utility>

//local
#include "GenExRndmWrapper.h"
#include "GenExSettings.h"
#include "GenExParticleInfo.h"
//HepMC 
#include "Event/HepMCEvent.h"
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"

//particle data (ROOT)
#include "TDatabasePDG.h"
#include "TLorentzVector.h"

//ntuples (ROOT)
#include "TTree.h"
#include "TFile.h"

//Foam (ROOT)
#include "TRandom3.h"
#include "TFoam.h"
#include "TFoamIntegrand.h"

//Gaudi (Status codes, errors)
#include "GaudiKernel/System.h"
#include "GaudiAlg/GaudiTool.h"

//implemented processes
#include "processes/JPsi.h"
#include "processes/Phi.h"



/*
	TODO:
		- rewriting amplitude calculations (pp->pVp for jpsi and phi works)
		- generator level cuts and statistics (started)
		- improve settings handling? (WIP)
		- error and exceptions handling (partially done)
		- remove unnecessary includes
*/


namespace GenEx{
/**	@class GenExGenerator GenExGenerator.h GenEx/GenExGenerator.h
*
*	TFoam based event generator for central exclusive production.
*
*	@author Mateusz Goncerz
*	@author based on GenEx generator by R. A. Kycia, J. Chwastowski, R. Staszewski, J. Turnau
*	@date 2017-07-15
*
*/


class GenExGenerator final : public GaudiTool{

	public:

		///default constructor disabled
		GenExGenerator() = delete;

		/**
		*	Default constructor
		*	
		*	Initializes the GaudiTool base class
		*	and pointer to Settings class shared with the interface.
		*
		*	First three parameters are for standard GaudiTool constructor,
		*	last two are GenEx::Settings class and a CM energy
		*/
		GenExGenerator(const std::string& type, const std::string& name, const IInterface* parent, GenExSettings& settings, Double_t CMEnergy);
		
		///default destructor
		virtual ~GenExGenerator();
		
		/**
		* 	Tool initialization
		*
		*	Calls basicSttgsCheck() and if everything's fine
		*	sets Foam settings, process type, random generator
		*	and calls Foam initialization method.
		*	
		*/
		virtual StatusCode initialize();

		/**
		*	Populates the event with particles
		*	based on process type and foam settings
		*/
		virtual StatusCode generateEvent(HepMC::GenEvent*);
		
		/**
		*	Calls Foam's Finalize() method and performs a cleanup
		*/
		virtual StatusCode finalize();

	private:

		///contains all the settings and is shared between generator and the interface
		GenExSettings& m_settings;
		
		///instance of TFoam
        	TFoam* m_foam = nullptr;

		///foam-friendly wrapper for gaudi random number generator 
        	GenExRndmWrapper m_randGen;  
 
		///process type (Foam density function)
		TFoamIntegrand* m_process = nullptr;
		
		///particles
 		GenExParticleInfo generatorParticles;
		///particle data
		TDatabasePDG m_particleData;

		///ntuple output
		TFile* m_outputFile = nullptr;
		TTree* m_outputTree = nullptr;
		std::vector<Double_t> m_outputValues;
};

}

#endif
