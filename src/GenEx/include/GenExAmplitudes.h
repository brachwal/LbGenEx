#ifndef GENEXAMPLITUDES_H
#define GENEXAMPLITUDES_H

#include <vector>
#include <string>
#include "TRandom3.h" //Double_t and Int_t

namespace GenEx{

enum class MesonType{Upsilon,JPsi,Phi,Rho,Omega};
enum class UGDFType{GBW,KL,BFKL,KutakStasto1,KutakStasto2,IvanovNikolaev,Gaussian};

namespace Amplitudes{


/*
class VPAmplitude{ /// doesnt work and needs overhaul

public:
	bool pVAmplitude(const MesonType::meson& mesonType, const UGDFType::UGDF& ugdfType);

private:
	Double_t blogw[400];
	Double_t brho[400];
	Double_t bimmat[400];
	
	Double_t alpha_s(const Double_t& Q2);
	void Zconvolution(const MesonType::meson& mesonType, const Double_t am_quark, const Double_t akappa2, const Double_t Q2, Double_t& Zconv0, Double_t& Zconv0_vec, Double_t& Zconv1, Double_t& Zconv1_vec);
	void WFconvolution(const MesonType::meson& mesonType, const Double_t am_quark, Double_t z,const Double_t Q2,const Double_t akappa2,Double_t& WFconv0,Double_t& WFconv0_vec,Double_t& WFconv1,Double_t& WFconv1_vec);
	Double_t radial_WF(const MesonType::meson& mesonType, const bool gauss0_coulomb1, Double_t p2);
	Double_t radialWFNorm(const MesonType::meson& mesonType, const bool gauss0_coulomb1, const Double_t& am_quark);

	Double_t GBW(const Double_t& x, const Double_t& akap2);
	Double_t KL(const Double_t x,const Double_t akap2, const Double_t& icut1, const Double_t& icut2);
	Double_t BFKL(const Double_t& x, const Double_t& akap2);
	Double_t DGD(const Double_t& xeff, const Double_t& ks, const Double_t& delta2, const Double_t& iglu);

void KUTAK_MAP( const Int_t& imode, Double_t (*fmap)[5001]);
Double_t KUTAK(Double_t (*fmap)[5001], const Double_t& x, const Double_t& akt2);
};
*/
class PVPAmplitude{ ///needs overhaul, works consistently with fortran code
					///need the Vp amp files in /GenEx/processes/amps
public:
	PVPAmplitude() = delete;
	PVPAmplitude(enum MesonType const&);
	
	Double_t calculatePVPAmplitude(bool absorption_corrections, Double_t s12, Double_t y, Double_t phi, Double_t xi1, Double_t xi2);
	//nb GeV^-4
private:

	Double_t am_meson;
	Double_t b0;
	Int_t nlogw = 400;
	

	std::vector<Double_t> blogw;
	std::vector<Double_t> brho;
	std::vector<Double_t> bimmat;

	void Absorption(Double_t& s,Double_t& zgamma,Double_t& z2,Double_t& t1min,Double_t& t2min,Double_t& p1x,Double_t& p1y,Double_t& p2x,Double_t& p2y,Double_t& ax,Double_t& ay,Double_t& re_ax,Double_t& re_ay);
	void azimuth(Double_t& q2,Double_t& s,Double_t& z1,Double_t& z2,Double_t& t1min,Double_t& t2min,Double_t& p1x,Double_t& p1y,Double_t& p2x,Double_t& p2y,Double_t& phimin,Double_t& phimax,Double_t& outx,Double_t& outy,Double_t& re_outx,Double_t& re_outy);
	void Two_to_Three(Double_t& s,Double_t& zgamma,Double_t& t1,Double_t& t2,Double_t& p1x,Double_t& p1y,Double_t& p2x,Double_t& p2y,Double_t& aborn_x,Double_t& aborn_y,Double_t& rho);
	void VMinterpol(Double_t& q2,Double_t& s,Double_t& t,Double_t& amp_im,Double_t& rho);
	Double_t dirac_f1(Double_t& q2);
	Double_t Pomeron(Double_t& s, Double_t& t);


};


}
}

#endif