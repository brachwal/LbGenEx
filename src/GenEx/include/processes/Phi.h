#ifndef PHI_H
#define PHI_H

#include "../GenExProcessBase.h"
#include "../GenExAmplitudes.h"

namespace GenEx{
namespace Process{

class Phi final : public GenExProcessBase{

public:

	Phi() = delete;
	~Phi(){}
	Phi(
		GenExRndmWrapper& randGen,
		GenExParticleInfo& generatorParticles,	
		GenExSettings::Kinematics const& settingsKinematics,	
		bool const& abs_corr)
		: GenExProcessBase(randGen, generatorParticles, settingsKinematics)
		, m_amplitude(Amplitudes::PVPAmplitude(MesonType::JPsi))
		, m_abs_corr(abs_corr)
		{}

	virtual Double_t calculateSquaredAmplitude() override{
		return m_amplitude.calculatePVPAmplitude(m_abs_corr, generatorParticles.m_CMEnergy, generatorParticles.m_meson4Vector.Rapidity(), generatorParticles.m_meson4Vector.Phi(), generatorParticles.m_outParticles.second[1].Pt(), generatorParticles.m_outParticles.second[2].Pt());
	}

private:
	Amplitudes::PVPAmplitude m_amplitude;
	bool m_abs_corr;			
};

}
}

#endif
