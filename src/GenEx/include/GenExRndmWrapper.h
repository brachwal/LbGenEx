#ifndef GENEXRNDMWRAPPER_H
#define GENEXRNDMWRAPPER_H
//#include <iostream>
#include "TRandom.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiAlg/GaudiTool.h"
//rndm rndmarray

namespace GenEx{

/**	@class GenExRndmWrapper GenExRndmWrapper.h GenEx/GenExRndmWrapper.h
*
*	TFoam friendly wrapper for gaudi random number service.
*	initializeDistribution() method has to be called every event before Foam's makeEvent()
*	@author Mateusz Goncerz
*	@date 2017-07-15
*
*/

class GenExRndmWrapper : public TRandom, public GaudiTool{

public:
	
	GenExRndmWrapper(const std::string& type, const std::string& name, const IInterface* parent) 
		: TRandom(0)
		, GaudiTool(type, name, parent)
		{m_randSvc = svc<IRndmGenSvc>("RndmGenSvc", true);}

	~GenExRndmWrapper() = default;


	StatusCode initialize() override {
		return GaudiTool::initialize();
	}
 			
	StatusCode finalize() override {
		return GaudiTool::finalize();
	}

	StatusCode initializeDistribution() {
		return m_flatDistribution.initialize(m_randSvc, Rndm::Flat(0,1));
	}

	///overrides TRandom functions
	Double_t Rndm() override {
		return m_flatDistribution();
	}
	void RndmArray(Int_t s, Float_t* a) override {
		for(Int_t i = 0; i < s; ++i)
			a[i] = m_flatDistribution();
	}
	void RndmArray(Int_t s, Double_t* a) override {
		for(Int_t i = 0; i < s; ++i)
			a[i] = m_flatDistribution();
	}

private:

	///Gaudi random service
	IRndmGenSvc* m_randSvc = nullptr;


	Rndm::Numbers m_flatDistribution;
};
}

#endif