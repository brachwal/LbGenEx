#ifndef GENEXPARTICLEINFO_H
#define GENEXPARTICLEINFO_H
#include "TLorentzVector.h"
#include <vector>

namespace GenEx{
struct GenExParticleInfo{
	Double_t m_CMEnergy; //GeV?
	/// 0 index not used
	///		id				4vector
	std::pair<std::vector<Int_t>, std::vector<TLorentzVector>> m_inParticles;
	std::pair<std::vector<Int_t>, std::vector<TLorentzVector>> m_outParticles;
	std::vector<Double_t> m_outParticlesMasses; ///this is in GeV
		
	///meson 4vector
	TLorentzVector m_meson4Vector;
		
	///is the meson decaying or not?
	bool m_decay = 0;
};
}
#endif