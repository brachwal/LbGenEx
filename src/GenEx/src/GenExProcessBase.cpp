#include "GenExProcessBase.h"
#include <iostream>
using namespace GenEx;

GenExProcessBase::~GenExProcessBase(){}

bool GenExProcessBase::applyGeometryConstraints() const{

	if(generatorParticles.m_meson4Vector.Pt() > 1.4) return 0;
	return 1; //to be implemented
}

Double_t GenExProcessBase::calculateSquaredAmplitude(){
	return 1; //default value if no amplitude defined by process
}

Double_t GenExProcessBase::Density(Int_t nDim, Double_t* Xarg){

	//create kinematics
	m_randGen.initializeDistribution();

	Double_t wtdecay = m_eventMaker.SetEvent(nDim, Xarg);

	if( m_eventMaker.isGenerationFailed() )
		return 0;
	else{


		if(!generatorParticles.m_decay) //if there's no decay, then meson is in [3]
			generatorParticles.m_meson4Vector = generatorParticles.m_outParticles.second[3];
		else //reconstruct meson from decay products
			generatorParticles.m_meson4Vector = [](std::vector<TLorentzVector> const& v){
					TLorentzVector a(0,0,0,0);
					for(unsigned int i = 3; i < v.size(); ++i)
						a += v[i];

					return a;
					}(generatorParticles.m_outParticles.second);

		wtdecay *= applyGeometryConstraints(); //0 if the event is rejected, 1 if not

		Double_t weight = wtdecay * calculateSquaredAmplitude(); //ampl is nb*GeV^-4
			weight *= 0.000001; //convert to mb*GeV^-4

			//convert GeV^-2 to mb if matrix element is in GeV^-2(natural units) - uncomment if needed
				//weight *= 0.3894;	
			
		return weight;
	}
}
