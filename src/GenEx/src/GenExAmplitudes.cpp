#include <cmath>
#include <iostream>
#include <fstream>

#include "GenExAmplitudes.h"


using namespace GenEx;
using namespace Amplitudes;

/*
		pp->Vp amplitude calculation, not working yet
*/
/*
bool VPAmplitude::pVAmplitude(const MesonType::meson& mesonType, const UGDFType::UGDF& ugdfType){

blogw.clear();
brho.clear();
bimmat.clear();


const Double_t alpha_em = 1./137.;
const Double_t pi = M_PI;

//Double_t imaginaryPart = 1;
//Double_t realPart = 1;

Double_t am_quark = -99999;
Double_t c_quark_flavour = -99999;

switch(mesonType){

	case MesonType::Upsilon:
	{
		am_meson = 9.4603;
		am_quark = 4.75;
		c_quark_flavour = 1./3.;
		b0 = 3.5;
		break;
	}
	case MesonType::JPsi:
	{
		am_meson = 3.097;
		am_quark = 1.5;
		c_quark_flavour = 2./3.;
		b0 = 4.88;
		break;
	}
	case MesonType::Phi:
	{
		am_meson = 1.020;
		am_quark = 0.45;
		c_quark_flavour = 1./3.;
		b0 = 7.5;
		break;
	}
	case MesonType::Rho:
	{
		am_meson = 0.775;
		am_quark = 0.3;
		c_quark_flavour = 1./sqrt(2.);
		b0 = 11.5;
		break;
	}
	case MesonType::Omega:
	{
		am_meson = 0.782;
		am_quark = 0.3;
		c_quark_flavour = 1./(3.*sqrt(2.));
		b0 = 11.5;
		break;
	}
	default:
	{
		return 0;	
	}
}
Double_t anorm = radialWFNorm(mesonType, 0, am_quark)*c_quark_flavour*sqrt(4.*pi*alpha_em)/(4*pi*pi);
Double_t amp = 0.939;
//Double_t amp2 = amp*amp;
Double_t W_min = amp + am_meson;
Double_t W_max = 2.e4;

Double_t ani_min = log(W_min);
Double_t ani_max = log(W_max);
unsigned int nani = 400;

Double_t dani = (ani_max - ani_min) / (Double_t)nani;

Double_t Q2 = 0; 

Double_t ani = ani_min;

///kutak
Double_t fmap[201][5001];

if(ugdfType == UGDFType::KutakStasto1)
	KUTAK_MAP(1, fmap);
else if(ugdfType == UGDFType::KutakStasto2)
	KUTAK_MAP(2, fmap);

for(unsigned int i = 0; i < nani; ++i, ani += dani){ ///energy loop

	Double_t W = exp(ani);
	Double_t W2 = W*W;
	Double_t c_skewed = 0.41;

	Double_t x = c_skewed*(Q2 + (am_meson*am_meson))/W2;

	Double_t frac = 0.1;

	Double_t x_plus = x + frac*x;
	Double_t x_minus = x - frac*x;
		
    Double_t sum0_p = 0;
    Double_t sum0_vec_p = 0;
    Double_t sum1_p = 0;
    Double_t sum1_vec_p = 0;
    Double_t sum0_m = 0;
    Double_t sum0_vec_m = 0;
    Double_t sum1_m = 0;
    Double_t sum1_vec_m = 0;

	Double_t am_quark2 = am_quark*am_quark;


	Double_t taumin = -10;
	Double_t taumax = 10;
	unsigned int ntau = 100;

	Double_t dtau = (taumax - taumin)/(Double_t)ntau;
	Double_t tau = taumin;

	unsigned int j = 1;
	Double_t c = -99999; 

	for(unsigned int i = 0; i < ntau ; ++i, ++j, tau += dtau){ ///integration over log(kappa^2)

		Double_t akappa2 = am_quark2 * exp(tau);

		if(j == 1) c = 2.;
		if(j == 2) c = 4.;

		if(i == 0) c = 1.;
		if(i == ntau-1) c = 1.;




		Double_t f_plus = -999999;
		Double_t f_minus = -999999;

		switch(ugdfType){
		
			case UGDFType::GBW:
			{
				f_plus = GBW(x_plus, akappa2) * akappa2;
				f_minus = GBW(x_minus, akappa2) * akappa2;
				break;
			}
			case UGDFType::KL:
			{
				f_plus = KL(x_plus, akappa2, 1, 1) * akappa2;
				f_minus = KL(x_minus, akappa2, 1, 1) * akappa2;
				break;
			}
			case UGDFType::BFKL:
			{
				f_plus = BFKL(x_plus, akappa2) * akappa2;
				f_minus = BFKL(x_minus, akappa2) * akappa2;
				break;
			}
			case UGDFType::KutakStasto1: ///not implemented (linear map)
			{
				f_plus = KUTAK(fmap, x_plus, akappa2) * akappa2;
				f_minus = KUTAK(fmap, x_minus, akappa2) * akappa2;
				break;
			}
			case UGDFType::KutakStasto2: ///this one was used in the fortran code
			{
				f_plus = KUTAK(fmap, x_plus, akappa2) * akappa2;
				f_minus = KUTAK(fmap, x_minus, akappa2) * akappa2;
				break;
			}
			case UGDFType::IvanovNikolaev: ///to be implemented later, too long and not used
			{
				f_plus = DGD(x_plus, akappa2, 0., 1);
				f_minus = DGD(x_minus, akappa2, 0., 1);
				break;
			}
			case UGDFType::Gaussian: ///f seems to not be used at all
			{
		//		f = 
			}
			default:
			{
				return 0;
			}	

		}

		f_plus /= akappa2*akappa2;
		f_minus /= akappa2*akappa2;




		Double_t Zconv0, Zconv0_vec, Zconv1, Zconv1_vec;

		Zconvolution(mesonType, am_quark, akappa2, Q2, Zconv0, Zconv0_vec, Zconv1, Zconv1_vec); ///to be implemented

         sum0_p = sum0_p + akappa2 * Zconv0 * f_plus * 2. * dtau * c / 3.;
         sum0_vec_p = sum0_vec_p + akappa2 * Zconv0_vec * f_plus * 2. *dtau*c / 3.;

         sum0_m = sum0_m + akappa2 * Zconv0 * f_minus * 2. * dtau * c / 3.;
         sum0_vec_m = sum0_vec_m + akappa2 * Zconv0_vec * f_minus * 2. * dtau * c / 3.;

         sum1_p = sum1_p + akappa2 * Zconv1 * f_plus * 2. * dtau * c / 3.;
         sum1_vec_p = sum1_vec_p + akappa2 * Zconv1_vec * f_plus * 2 * dtau * c /3.;

         sum1_m = sum1_m + akappa2 * Zconv1 * f_minus * 2. * dtau * c / 3.;
         sum1_vec_m = sum1_vec_m + akappa2 * Zconv1_vec * f_minus * 2. * dtau * c / 3.;
		 
		if(j == 2) j = 0;

	} //end of integration over log(kappa^2)

    //Double_t ampli0_p     = anorm * pi * sum0_p;
    Double_t ampli0_vec_p = anorm * pi * sum0_vec_p;
    //Double_t ampli1_p     = anorm * pi * sum1_p;
    Double_t ampli1_vec_p = anorm * pi * sum1_vec_p;
    //Double_t ampli0_m     = anorm * pi * sum0_m;
    Double_t ampli0_vec_m = anorm * pi * sum0_vec_m;
    //Double_t ampli1_m     = anorm * pi * sum1_m;
    Double_t ampli1_vec_m = anorm * pi * sum1_vec_m;
    Double_t a_plus = ampli0_vec_p + ampli1_vec_p;
    Double_t a_minus = ampli0_vec_m + ampli1_vec_m;
    Double_t delt = -pi/2. * ((a_plus - a_minus) / ((a_plus + a_minus)/2.)) / log(x_plus/x_minus);
	Double_t rho2 = tan(delt);

    Double_t ampli0_vec = (ampli0_vec_p + ampli0_vec_m)/2.;
    Double_t ampli1_vec = (ampli1_vec_p + ampli1_vec_m)/2.;
	
	Double_t bc = log(fabs(ampli0_vec + ampli1_vec));

	
	blogw.push_back(ani);
	brho.push_back(rho2);
	bimmat.push_back(bc);
}
return 1;
}

Double_t VPAmplitude::alpha_s(const Double_t& Q2){ ///taken from dgd.f
Double_t pi=atan(1.)*4.;
Double_t lqcd2=0.04;
Double_t Q2s = std::max(Q2,0.22);
Double_t a_s = 4.*pi/9./log(Q2s/lqcd2);
a_s = std::min(0.82,a_s);

return a_s;
}

void VPAmplitude::Zconvolution(const MesonType::meson& mesonType, const Double_t am_quark, const Double_t akappa2, const Double_t Q2, Double_t& Zconv0, Double_t& Zconv0_vec, Double_t& Zconv1, Double_t& Zconv1_vec){

	Double_t sum0 = 0.;
	Double_t sum0_vec = 0.;

	Double_t sum1 = 0.;
	Double_t sum1_vec = 0.;

	Double_t zmin = 0.;
	Double_t zmax = 1;

	unsigned int nz = 40;

	Double_t dz = (zmax - zmin)/(Double_t)nz;

	Double_t z = zmin;
	unsigned int j = 1;
	Double_t c = -9999;

	for(unsigned int i = 0; i < nz; ++i, ++j, z += dz){
		
		if(j == 1) c = 2.;
		if(j == 2) c = 4.;
		if(i == 0) c = 1.;
		if(i == nz-1) c = 1.;

		Double_t factor = 1./ (z + 1e-7) / (1. + 1e-7 - z);

		Double_t WFconv0(0), WFconv0_vec(0), WFconv1(0), WFconv1_vec(0);


		WFconvolution(mesonType, am_quark, z, Q2, akappa2, WFconv0, WFconv0_vec, WFconv1, WFconv1_vec);

        sum0     = sum0     + WFconv0 * factor * dz * c / 3.;
        sum0_vec = sum0_vec + WFconv0_vec * factor * dz * c / 3.;
        sum1     = sum1     + WFconv1 * factor * dz * c / 3.;
        sum1_vec = sum1_vec + WFconv1_vec * factor * dz * c /3.;		

		if(j == 2) j = 0;
	}

	Zconv0 = sum0;
	Zconv0_vec = sum0_vec;
	Zconv1 = sum1;
	Zconv1_vec = sum1_vec;
}


void VPAmplitude::WFconvolution(const MesonType::meson& mesonType, const Double_t am_quark, Double_t z,const Double_t Q2,const Double_t akappa2,Double_t& WFconv0,Double_t& WFconv0_vec,Double_t& WFconv1,Double_t& WFconv1_vec){


	///helper lambdas

	auto theta = [](const Double_t& ak2, const Double_t& akappa2, const Double_t& eps2)->Double_t{
		Double_t root = sqrt( (ak2 - eps2 - akappa2)*(ak2 - eps2 - akappa2) + 4.*ak2*eps2);

		Double_t th = 0.;

		if(ak2 != 0){
			th = (ak2+eps2) / (2.*ak2) * (1. + (ak2-eps2-akappa2)/root);
		}
		
		return th;
	};

	auto W0 = [](const Double_t& ak2, const Double_t& akappa2, const Double_t& eps2)->Double_t{
		Double_t root = sqrt( (ak2 - eps2 - akappa2)*(ak2 - eps2 - akappa2) + 4.*ak2*eps2);
		
		return 1./(ak2+eps2) - 1./root;
	};

	auto W1 = [&theta](const Double_t& ak2, const Double_t& akappa2, const Double_t& eps2)->Double_t{
		return 1. - theta(ak2, akappa2, eps2);
	};


	Double_t am_quark2 = am_quark*am_quark;
	Double_t pi = M_PI;

	Double_t taumin = -15.;
	Double_t taumax = 1.;
	unsigned int ntau = 100;

	Double_t dtau = (taumax - taumin)/(Double_t)ntau;
	Double_t tau = taumin;

	Double_t sum0 = 0.;
	Double_t sum0_vec = 0.;
	Double_t sum1 = 0.;
	Double_t sum1_vec = 0.;

	Double_t c = 0;

	unsigned int j = 1;




	for(unsigned int i = 0; i < ntau; ++i, ++j, tau += dtau){ //integral on log(kappa^2)

		Double_t ak2 = am_quark2 * exp(tau);

		if(j == 1) c = 2.;
		if(j == 2) c = 4.;
		if(i == 0) c = 1.;
		if(i == ntau - 1) c = 1.;
		
		Double_t amqq2 = (ak2+am_quark2)/(z+1.e-7)/(1. + 1.e-7 - z);
		Double_t eps2 = am_quark2 + (z*(1. - z)*Q2);

		Double_t A0 = am_quark2;
		Double_t dA0 = ak2*am_quark / (sqrt(amqq2) + 2.*am_quark);
		Double_t A1 = (1. - 2.*z*(1.-z))*ak2/(ak2+eps2);
		Double_t dA1 = -(2.*z - 1.)*(2.*z - 1.) * am_quark / (sqrt(amqq2) + 2.*am_quark) * ak2 / (ak2+eps2);

		Double_t p2 = (amqq2 - 4. * am_quark2) / 4.;
		Double_t psi = radial_WF(mesonType, 0, p2);

		Double_t q2_eff = std::max(akappa2, ak2 + eps2);

		Double_t as = alpha_s(q2_eff);

        sum0     = sum0 + ak2*psi*as*A0*W0(ak2,akappa2,eps2)*dtau*c/3.;
        sum0_vec = sum0_vec + ak2*psi*as*(A0+dA0)*W0(ak2,akappa2,eps2)*dtau*c/3.;

        sum1     = sum1 + ak2*psi*as*A1*W1(ak2,akappa2,eps2)*dtau*c/3.;
        sum1_vec = sum1_vec + ak2*psi*as*(A1+dA1)*W1(ak2,akappa2,eps2)*dtau*c/3.;

		if(j == 2) j = 0;	
	}

	WFconv0     = pi*sum0;
    WFconv0_vec = pi*sum0_vec;
    WFconv1     = pi*sum1;
    WFconv1_vec = pi*sum1_vec;

}


Double_t VPAmplitude::radial_WF(const MesonType::meson& mesonType, const bool gauss0_coulomb1, Double_t p2){
	
	
	Double_t r2 = -9999;
	Double_t aksi2 = -9999;
	Double_t a2 = -99999;	
	
switch(mesonType){

	case MesonType::Upsilon:
	{
        r2 = 0.474;
        aksi2=1.825;
        a2=1.195*r2;		
		break;
	}
	case MesonType::JPsi:
	{
        r2 = 2.741;
        aksi2=1.825;
        a2=1.195*r2;
		break;
	}
	case MesonType::Phi:
	{
        r2 = 12.85;
        aksi2=1.825;
        a2=1.195*r2;
		break;
	}
	case MesonType::Rho:
	{
        r2 = 16.51;
        aksi2=1.825;
        a2=1.195*r2;
		break;
	}
	case MesonType::Omega:
	{
        r2 = 18.90;
        aksi2=1.825;
        a2=1.195*r2;
		break;
	}
	default:{
		return -999999;
	}
}	

if(gauss0_coulomb1 == 0)
	return exp(-r2*p2/2.);
else if(gauss0_coulomb1 == 1)
	return (aksi2 - (a2*p2))*exp((-a2*p2)/2.);

return -9999;
}

Double_t VPAmplitude::radialWFNorm(const MesonType::meson& mesonType, const bool gauss0_coulomb1, const Double_t& am_quark){

	Double_t r2 = -9999;
	Double_t aksi2 = -9999;
	Double_t a2 = -99999;

switch(mesonType){

	case MesonType::Upsilon:
	{
        r2 = 0.474;
        aksi2=1.825;
        a2=1.195*r2;		
		break;
	}
	case MesonType::JPsi:
	{
        r2 = 2.741;
        aksi2=1.825;
        a2=1.195*r2;
		break;
	}
	case MesonType::Phi:
	{
        r2 = 12.85;
        aksi2=1.825;
        a2=1.195*r2;
		break;
	}
	case MesonType::Rho:
	{
        r2 = 16.51;
        aksi2=1.825;
        a2=1.195*r2;
		break;
	}
	case MesonType::Omega:
	{
        r2 = 18.90;
        aksi2=1.825;
        a2=1.195*r2;
		break;
	}
	default:{
		return -999999;
	}
}	


	Double_t pi = M_PI;

	Double_t amu = am_quark;
	Double_t anc = 3.;

	Double_t taumin = -5.;
	Double_t taumax = 10.;
	unsigned int ntau = 100;

	Double_t dtau = (taumax - taumin)/(Double_t)ntau;

    Double_t sum_norm = 0.;
	Double_t sum_width = 0.;

	Double_t tau = taumin;

	int j = 1;

	Double_t c;

if(gauss0_coulomb1 == 0){

	for(unsigned int i = 1; i < ntau + 1; ++i, ++j, tau += dtau){

		Double_t p = amu * exp(tau);
		
		if(j == 1) c = 2.;
		if(j == 2) c = 4.;
		if(i == 1) c = 1.;
		if(i == ntau) c = 1.;

		Double_t p2 = p*p;
		Double_t amqq2 = 4. * p2 + 4. * am_quark*am_quark;
		//Double_t amqq = 2.*sqrt(p2+am_quark*am_quark);

		Double_t ajacobian = p;
		Double_t psi = exp(-r2*p2/2.);


		Double_t factor_width=4.*pi*anc*8./3./pow((2.*pi),3)*(sqrt(amqq2)+am_quark);
		Double_t factor_norm = anc*4.*pi*4.*sqrt(amqq2)/pow((2.*pi),3);
		sum_norm = sum_norm + ajacobian*p2*pow(psi,2)*factor_norm*dtau*c/3.;
		sum_width = sum_width + ajacobian*p2*psi*factor_width*dtau*c/3.;

		if(j == 2) j = 0;
	}

}else if(gauss0_coulomb1 == 1){ ///?????????????????????????

	for(unsigned int i = 1; i < ntau + 1; ++i, ++j, tau += dtau){

		Double_t p = amu * exp(tau);
		
		if(j == 1) c = 2.;
		if(j == 2) c = 4.;
		if(i == 1) c = 1.;
		if(i == ntau) c = 1.;

		Double_t p2 = p*p;
		Double_t amqq2 = 4. * p2 + 4. * am_quark*am_quark;

		Double_t ajacobian = p;
		Double_t psi = ( (aksi2 - (a2*p2)) * exp( -a2*p2 / 2.) );
        Double_t factor_width = 4.*pi*anc*8./3./pow((2.*pi),3)*(sqrt(amqq2)+am_quark);
        Double_t factor_norm = anc*4.*pi*4.*sqrt(amqq2)/pow((2.*pi),3);
        sum_norm = sum_norm + ajacobian*p2*psi*psi*factor_norm*dtau*c/3.;
        sum_width = sum_width + ajacobian*p2*psi*factor_width*dtau*c/3.;



		if(j == 2) j = 0;
	}	

}
	Double_t xnorm_WF = 1./sqrt(sum_norm);
	//Double_t gv = sum_width*xnorm_WF;
	
	return xnorm_WF;

}



///////////////////UGDFs/////////////////////

Double_t VPAmplitude::GBW(const Double_t& x, const Double_t& akap2){
	Double_t pi = 4.*atan(1.);
	Double_t units = 10.*(197.3271*197.3271)/1.e6;
	Double_t alpha_s = 0.2;
	Double_t sigma_0 = 29.12/units;
	Double_t alam = 0.277;
	Double_t x0 = 0.41*1.e-4;
	Double_t R0 = pow((x/x0), alam);
	Double_t arg = R0*R0*akap2;
	
	return 3.*sigma_0/4./pi*pi*R0*R0*akap2*exp(-arg)  * pow((1.-x),7) / alpha_s;	
}

Double_t VPAmplitude::KL(const Double_t x,const Double_t akap2, const Double_t& icut1, const Double_t& icut2){
	
    Double_t pi = 4.*atan(1.);
	Double_t alam = 0.288;
	Double_t x0 = 3.04*1.e-4;
	Double_t Q02 = 1.0;
	Double_t Q02s = Q02*pow((x0/x),alam);
	Double_t units = 10.*(197.3271*197.3271)/1.e+6;
	Double_t phi0 = 170./units/4./sqrt(2.)/pow(pi,3); 	
	
	if(akap2 <= Q02s)
		return phi0 * pow((1.-x),7) * icut1;
	else if(akap2 > Q02s)
		return phi0* (Q02s/akap2)     * pow((1.-x),7)  * icut2;
	

	return -99999;
}

Double_t VPAmplitude::BFKL(const Double_t& x, const Double_t& akap2){
	
Double_t pi = 4.*atan(1.);
Double_t dzeta_3 = 1.202;
Double_t alpha_s = 0.2;
Double_t alpha_sb = 3.*alpha_s/pi;
Double_t alam = 4.*alpha_sb*log(2.);

Double_t cnorm = 1.19   ;            
Double_t q02 = 1.0;
Double_t qb2 = 1.0;
Double_t alambis = 28.*alpha_sb*dzeta_3;
Double_t r = 0.15;

Double_t term1 = 1./sqrt(2.*pi*alambis*log(1./x));
Double_t term2 = exp( -pow(log(akap2/qb2),2) / (r*2.*alambis*log(1./x)) );

return cnorm/pow(x,alam) * sqrt(akap2/q02) * term1 * term2 * pow(1.-x,7) / akap2;
	
}

Double_t VPAmplitude::DGD(const Double_t& xeff, const Double_t& ks, const Double_t& delta2, const Double_t& iglu) {
	
	return -99999; ///not implemented yet, long and not used
	
}

////kutak
void VPAmplitude::KUTAK_MAP( const Int_t& imode, Double_t (*fmap)[5001]){
	

	Double_t nx = 80;
	Double_t nkt2 = 1000;

	
	for(int i = 0; i < 201; ++i)
		for(int j = 0; j < 5001; ++j)
			fmap[i][j] = 0.0;
	
	std::ifstream file;
	if( imode == 1)
		file.open("grid_kutak_linear.dat");		
	else if(imode == 2)
		file.open("grid_kutak_nonlinear.dat");
	
	std::string tmp;
	
	for(int i = 1; i < nx + 1; ++i){
		for(int j = 1; j < nkt2 + 1; ++j){
			std::getline(file, tmp);
			fmap[i][j] = std::stod(tmp);
		}
	}
	
	file.close();
}

Double_t VPAmplitude::KUTAK(Double_t (*fmap)[5001], const Double_t& x, const Double_t& akt2){
	
Double_t alogx_min = -8.;
Double_t alogx_max =  0.;
Double_t nx = 80;
Double_t dlogx = (alogx_max-alogx_min)/nx;

Double_t akt2_min =  0.0;
Double_t akt2_max =  100.0;
Double_t nkt2 = 1000;
Double_t dkt2 = (akt2_max-akt2_min)/nkt2;

Double_t alogx = log10(x); 	
	
	
if(alogx < (alogx_min+dlogx/2.) || alogx > (alogx_max-dlogx/2.))
    return 0.0;


if(akt2 < (akt2_min+dkt2/2.) || akt2 > (akt2_max-dkt2/2.))
	return 0.0;
    
Double_t dellogx = (alogx-alogx_min);
Double_t delkt2 = (akt2-akt2_min);


Double_t sx = (dellogx-dlogx/2.) / dlogx;
Double_t ix = (dellogx-dlogx/2.) / dlogx;
Double_t ix_lo = ix+1;
Double_t ix_up = ix_lo+1;


Double_t skt2 = (delkt2-dkt2/2.) / dkt2;
Double_t ikt2 = (delkt2-dkt2/2.) / dkt2; 
Double_t ikt2_lo = ikt2+1;
Double_t ikt2_up = ikt2_lo+1;



//Double_t alogx_lo = alogx_min+ix_lo*dlogx-dlogx/2.;
//Double_t alogx_up = alogx_min+ix_up*dlogx-dlogx/2.;
//Double_t akt2_lo = akt2_min+ikt2_lo*dkt2-dkt2/2.;
//Double_t akt2_up = akt2_min+ikt2_up*dkt2-dkt2/2.;


Double_t f11 = fmap[static_cast<Int_t>(ix_lo)][static_cast<Int_t>(ikt2_lo)];
Double_t f12 = fmap[static_cast<Int_t>(ix_up)][static_cast<Int_t>(ikt2_lo)];
Double_t f21 = fmap[static_cast<Int_t>(ix_lo)][static_cast<Int_t>(ikt2_up)];
Double_t f22 = fmap[static_cast<Int_t>(ix_up)][static_cast<Int_t>(ikt2_up)];



Double_t ddlogx = (sx - (Double_t)ix)*dlogx;
Double_t ddkt2  = (skt2 - (Double_t)ikt2)*dkt2;


Double_t w11 = (dlogx-ddlogx)/dlogx * (dkt2-ddkt2)/dkt2;
Double_t w12 =       ddlogx  /dlogx * (dkt2-ddkt2)/dkt2;
Double_t w21 = (dlogx-ddlogx)/dlogx *      ddkt2  /dkt2;
Double_t w22 =       ddlogx  /dlogx *      ddkt2  /dkt2;


Double_t f_int =  w11*f11 + w12*f12 + w21*f21 + w22*f22;

return f_int;
}
*/

/*
	pp->pVp amplitude calculation
*/

PVPAmplitude::PVPAmplitude(enum MesonType const& mesonType){
	std::cout<< "\t[DEBUG]:: Instantiating PVPAmplitude" <<std::endl;
	std::ifstream f;

	//const char* env_p = std::getenv("GENEXROOT");
	std::string data_path = PROCESSES_DATA_LOCATION;
	std::cout<< "\t[DEBUG]:: PROCESSES_DATA_LOCATION " << data_path <<std::endl;

	switch(mesonType){

	case MesonType::Upsilon:
	{
		am_meson = 9.4603;
		b0 = 3.5;
		break;
	}
	case MesonType::JPsi:
	{
		//f.open((std::string(env_p)+"/GenEx/processes/Vp_amplitudes/ampli_JPsi.dat").c_str());
		
		f.open((data_path+"/ampli_JPsi.dat").c_str());
		am_meson = 3.097;
		b0 = 4.88;
		break;
	}
	case MesonType::Phi:
	{
		// f.open((std::string(env_p)+"/GenEx/processes/Vp_amplitudes/ampli_Phi.dat").c_str());
		f.open((data_path+"/ampli_Phi.dat").c_str());
		am_meson = 1.020;
		b0 = 7.5;
		break;
	}
	case MesonType::Rho:
	{
		am_meson = 0.775;
		b0 = 11.5;
		break;
	}
	case MesonType::Omega:
	{
		am_meson = 0.782;
		b0 = 11.5;
		break;
	}
	//default: ///this should probably throw an error
	}
	

	std::string l;
	
	while(getline(f, l)){
		blogw.push_back(stod(l.substr(1,8))*pow(10,stod(l.substr(10,3))));
		brho.push_back(stod(l.substr(17,8))*pow(10,stod(l.substr(26,3))));
		bimmat.push_back(stod(l.substr(33,8))*pow(10,stod(l.substr(42,3))));
	}
	f.close();	

}	
	


Double_t PVPAmplitude::calculatePVPAmplitude(bool absorption_corrections, Double_t s12, Double_t y, Double_t phi, Double_t xi1, Double_t xi2){
/*std::cout << "y" << y << std::endl;
std::cout << "phi" << phi << std::endl;
std::cout << "xi1" << xi1 << std::endl;
std::cout << "xi2" << xi2 << std::endl;
*/

	Int_t nlogw = 400;
	Double_t s = s12*s12;
	Double_t amn = 0.939;
	Double_t amu2 = 0.1;
    Double_t ep = 1;
	Double_t pi = M_PI;
	
	Double_t angle = -99999;

	if (y < 0)
		angle = -phi/2.;
	else if (y > 0) 
		angle =  phi/2.;
	else if (y == 0.) 
		angle = phi/2.;
				
	Double_t plsq = amu2 * exp(xi1);
	Double_t p1 = sqrt(plsq);
	Double_t p1x = p1*cos(angle);
	Double_t p1y = p1*sin(angle);
				

				
	Double_t p2sq = amu2*exp(xi2);
	Double_t p2 = sqrt(p2sq);
	Double_t p2x = p2*cos(-angle);
	Double_t p2y = p2*sin(-angle);
				

    Double_t pvmx = - p2x - p1x;
    Double_t pvmy = - p2y - p1y;
    Double_t pvm2 = pvmx*pvmx + pvmy*pvmy;       
    Double_t z1 = sqrt(pvm2 + am_meson*am_meson)/s12 * exp(y);
    Double_t z2 = sqrt(pvm2 + am_meson*am_meson)/s12 * exp(-y);					


    if(z1 > 1 || z2 > 1) return 0;
					
    Double_t t1 = - ( pow(p1,2)  +  pow(z1,2) *amn*amn) /(1.+1.e-17 - z1);
    Double_t t2 = - ( pow(p2,2)  +  pow(z2,2) *amn*amn) /(1.+1.e-17 - z2);
    Double_t t1min = - pow(z1,2) *amn*amn /(1.+1.e-17 - z1);
    Double_t t2min = - pow(z2,2) *amn*amn /(1.+1.e-17 - z2);
			

    Double_t tmax = 10.;
    Double_t abst1= fabs(t1);
    Double_t abst2= fabs(t2);

    Double_t agampom_x = 0.;
    Double_t agampom_y = 0.;
    Double_t rho_gampom = 0.;
    Double_t apomgam_x = 0.;
    Double_t apomgam_y = 0.;
    Double_t rho_pomgam = 0.;
	Double_t re_dagampom_x = 0.;
	Double_t re_dagampom_y = 0.;
	Double_t dagampom_x = 0.;
	Double_t dagampom_y = 0.;
	Double_t dapomgam_x = 0.;
	Double_t dapomgam_y = 0.;
	Double_t re_dapomgam_x = 0.;
	Double_t re_dapomgam_y = 0.;

    if(abst1 > tmax || abst2 > tmax)
		return 0;
    else{

		Two_to_Three(s,z1,t1,t2,p1x,p1y,p2x,p2y,agampom_x,agampom_y,rho_gampom);
		Two_to_Three(s,z2,t2,t1,p2x,p2y,p1x,p1y,apomgam_x,apomgam_y,rho_pomgam);

		if(absorption_corrections){
			Absorption(s,z1,z2,t1min,t2min,p1x,p1y,p2x,p2y,dagampom_x,dagampom_y,re_dagampom_x,re_dagampom_y);
			Absorption(s,z2,z1,t2min,t1min,p2x,p2y,p1x,p1y,dapomgam_x,dapomgam_y,re_dapomgam_x,re_dapomgam_y);
		}
	}

    Double_t tborn_pp_x = ep * agampom_x + ep * apomgam_x;
    Double_t tborn_pp_y = ep * agampom_y + ep * apomgam_y;			
    Double_t re_tborn_pp_x = ep*rho_gampom*agampom_x + ep*rho_pomgam*apomgam_x;
    Double_t re_tborn_pp_y = ep*rho_gampom*agampom_y + ep*rho_pomgam*apomgam_y;


    Double_t t_pp_x = ep*(agampom_x - dagampom_x) + ep*(apomgam_x - dapomgam_x);

    Double_t t_pp_y = ep*(agampom_y - dagampom_y) + ep*(apomgam_y - dapomgam_y);

    Double_t re_t_pp_x = ep*(rho_gampom*agampom_x - re_dagampom_x) + ep*(rho_pomgam*apomgam_x - re_dapomgam_x);

    Double_t re_t_pp_y = ep*(rho_gampom*agampom_y - re_dagampom_y) + ep*(rho_pomgam*apomgam_y - re_dapomgam_y);
		

    Double_t a_pp_x = t_pp_x;
    Double_t a_pp_y = t_pp_y;
    Double_t re_a_pp_x = re_t_pp_x;
    Double_t re_a_pp_y = re_t_pp_y;


    Double_t aborn_pp_x = tborn_pp_x;
    Double_t aborn_pp_y =  tborn_pp_y;
    Double_t re_aborn_pp_x = re_tborn_pp_x;
    Double_t re_aborn_pp_y = re_tborn_pp_y;


    Double_t units = 0.38935 * 1.e6;   
    Double_t factor = 1./512./pow(pi,4.)/pow(s,2)* units;      //now nb GeV^-4

	if(absorption_corrections)
		return factor*(a_pp_x*a_pp_x + re_a_pp_x*re_a_pp_x + a_pp_y*a_pp_y + re_a_pp_y*re_a_pp_y);
	else
		return factor*(aborn_pp_x*aborn_pp_x + re_aborn_pp_x*re_aborn_pp_x + aborn_pp_y*aborn_pp_y + re_aborn_pp_y*re_aborn_pp_y);
}

void PVPAmplitude::Absorption(Double_t& s,Double_t& zgamma,Double_t& z2,Double_t& t1min,Double_t& t2min,Double_t& p1x,Double_t& p1y,Double_t& p2x,Double_t& p2y,Double_t& ax,Double_t& ay,Double_t& re_ax,Double_t& re_ay){
	
	Double_t pi = M_PI;
	
	Double_t ak2max = 10.*2./b0;
	Double_t amu2 = 0.1;
	Double_t ximin = -10.;
	Double_t ximax = log(ak2max/amu2);
    Int_t nxi = 50;      
	Double_t dxi = (ximax - ximin) / static_cast<Double_t>(nxi);

	Double_t sumx=0.;
	Double_t sumy=0.;
	Double_t re_sumx=0.;
	Double_t re_sumy=0.;

    Int_t j = 1;
	Double_t xi = ximin;
	
	Double_t c;

	for(auto ixi = 0; ixi < nxi + 1; ++j, ++ixi, xi += dxi){
		
		if(j == 1) c = 2.;
		if(j == 2) c = 4.;
		if(ixi == 0) c = 1.;
		if(ixi == nxi) c = 1.;
		
        Double_t q2 = amu2 * exp(xi);
        Double_t ajacobian = q2;
        Double_t phimin = 0.;
        Double_t phimax = 2.*pi;
        Double_t a_phi_x = 0;
        Double_t a_phi_y = 0;
        Double_t re_a_phi_x = 0;
        Double_t re_a_phi_y = 0;
		
		
		
        azimuth(q2,s,zgamma,z2,t1min,t2min,p1x,p1y,p2x,p2y,phimin,phimax,a_phi_x,a_phi_y,re_a_phi_x,re_a_phi_y);
        sumx = sumx + pi*ajacobian*dxi*a_phi_x*c/3.;
        sumy = sumy + pi*ajacobian*dxi*a_phi_y*c/3.;
        re_sumx = re_sumx+pi*ajacobian*dxi*re_a_phi_x*c/3.;
        re_sumy = re_sumy+pi*ajacobian*dxi*re_a_phi_y*c/3.;

        if (j == 2) j=0;		
	}
	
    ax = sumx;
    ay = sumy;
    re_ax = re_sumx;
    re_ay = re_sumy;
}

void PVPAmplitude::azimuth(Double_t& q2,Double_t& s,Double_t& z1,Double_t& z2,Double_t& t1min,Double_t& t2min,Double_t& p1x,Double_t& p1y,Double_t& p2x,Double_t& p2y,Double_t& phimin,Double_t& phimax,Double_t& outx,Double_t& outy,Double_t& re_outx,Double_t& re_outy){
		 
	Double_t pi = M_PI;

	Double_t amn = 0.939;
	Double_t amn2 = amn*amn;
	Double_t q = sqrt(q2);
	Int_t n = 40;
	Double_t a=phimin;
	Double_t b=phimax;
	Double_t dphi=(b-a)/static_cast<Double_t>(n);
	Int_t j = 1;

	Double_t sx=0.;
	Double_t sy=0.;
	Double_t re_sx=0.;
	Double_t re_sy=0.;
	
	Double_t phi = a;
	Double_t c;
	
	for(auto k = 0; k < n + 1; ++k, ++j, phi += dphi){
		
	    if(j == 1) c=2.;
        if(j == 2) c=4.;
        if(k == 0) c=1.;
        if(k == n) c=1.;	
		
		Double_t akappax=q*cos(phi);
		Double_t akappay=q*sin(phi);
		Double_t ak1x=akappax;
		Double_t ak1y=akappay;
		Double_t ak2x=p1x + p2x - akappax;
		Double_t ak2y=p1y + p2y - akappay;
		Double_t t1 = - (ak1x*ak1x + ak1y*ak1y + pow(z1,2) *amn2)/(1.+1.e-17 - z1);
		Double_t t2 = - (ak2x*ak2x + ak2y*ak2y + pow(z2,2) *amn2)/(1.+1.e-17 - z2);
		Double_t tpom = - pow((p1x - akappax),2) - pow((p1y - akappay),2);
		Double_t aborn_x = 0.;
		Double_t aborn_y = 0.;
		Double_t rho = 0.;		
		
		
        Two_to_Three(s,z1,t1,t2,ak1x,ak1y,ak2x,ak2y,aborn_x,aborn_y,rho);
		Double_t Tabs = Pomeron(s,tpom)/2./pow((2.*pi),2);

        sx=sx+ Tabs*aborn_x*dphi*c/3.;
        sy=sy+ Tabs*aborn_y*dphi*c/3.;
        re_sx= re_sx+ Tabs*rho*aborn_x*dphi*c/3.;
        re_sy= re_sy+ Tabs*rho*aborn_y*dphi*c/3.;

        if(j == 2) j=0;			
	}
	
	
    outx=sx/(2.*pi);
    outy=sy/(2.*pi);
    re_outx=re_sx/(2.*pi);
    re_outy=re_sy/(2.*pi);
}

void PVPAmplitude::Two_to_Three(Double_t& s,Double_t& zgamma,Double_t& t1,Double_t& t2,Double_t& p1x,Double_t& p1y,Double_t& p2x,Double_t& p2y,Double_t& aborn_x,Double_t& aborn_y,Double_t& rho){
	
	Double_t pi = M_PI;
	
	Double_t alpha_em = 1. / 137.;
	Double_t g_em = sqrt(4.*pi*alpha_em);
	Double_t sgp = zgamma*s;
	Double_t q2 = -t1;
	Double_t ff = dirac_f1(q2);
	Double_t vm = 0;

    VMinterpol(q2,sgp,t2,vm,rho);
	aborn_x = g_em* ff * 2./zgamma * p1x / t1 * vm; 
	aborn_y = g_em* ff * 2./zgamma * p1y / t1 * vm; 
}

void PVPAmplitude::VMinterpol(Double_t& q2,Double_t& s,Double_t& t,Double_t& amp_im,Double_t& rho){
	
	
	Double_t alpha_prime = 0.25;

	Double_t xn = 1.;
	Double_t s0 = pow((95.),2.);
	Double_t am_meson2 = am_meson * am_meson;
	Double_t prefac = pow((am_meson2 / (q2 + am_meson2)),xn);

	Int_t npoints = 399;          
	Double_t flogW_min = 0.67243;             
	Double_t flogW_max = 0.98804*10;
	Double_t dellogW = (flogW_max - flogW_min)/static_cast<Double_t>(npoints);

	Double_t W = sqrt(s);
	Double_t flogW = log(W);	
	
	
	if(flogW < flogW_min || flogW > flogW_max){
		
        amp_im = 0.0;
        rho = 0.0;		
	} else{
		
	Double_t distance = flogW-flogW_min;

	Int_t ilower = static_cast<Int_t>(distance/dellogW) + 1;
	Int_t iupper = ilower + 1;
	
    if (iupper > npoints) iupper = npoints;


	Double_t flogW_lower = blogw[ilower -1];
	Double_t flogW_upper = blogw[iupper -1];

	Double_t rho_lower = brho[ilower -1];
	Double_t rho_upper = brho[iupper -1];


	Double_t amp_im_lower = bimmat[ilower -1];
	Double_t amp_im_upper = bimmat[iupper -1];

	Double_t x = flogW - flogW_lower;

	Double_t amp_im_temp = amp_im_lower + x/dellogW*(amp_im_upper-amp_im_lower);
	Double_t rho_temp = rho_lower + x/dellogW*(rho_upper-rho_lower);

	Double_t amp_im_t = exp(amp_im_temp);

	amp_im = s*amp_im_t * prefac * exp(b0*t/2.)* exp(t*alpha_prime*log(s/s0));

    rho = rho_temp;
	}
}

Double_t PVPAmplitude::dirac_f1(Double_t& q2){
	
	Double_t q02 = 0.71;
	Double_t amn = 0.939;
	Double_t amn2 = amn*amn;
	Double_t prefac = (4. * amn2 + 2.79 * q2)/(4. * amn2 + q2);
    
	return prefac/pow((1. + q2/q02),2); 	
	
}

Double_t PVPAmplitude::Pomeron(Double_t& s, Double_t& t){
		Double_t Bel = 21.;
		Double_t sigma = 100. * (1./0.38935);
		return sigma*exp(Bel*t/2.);
}
