#include "GenExGenerator.h"
#include <iostream>
using namespace GenEx;

GenExGenerator::GenExGenerator(const std::string& type, const std::string& name, const IInterface* parent, GenExSettings& settings, Double_t CMEnergy)
	: GaudiTool(type, name, parent)
	, m_settings(settings)
	, m_randGen("", "GenExRndmWrapperInstance", this)
	{generatorParticles.m_CMEnergy = CMEnergy;}

GenExGenerator::~GenExGenerator(){}

StatusCode GenExGenerator::initialize(){

	info() << "GenEx::Initializing generator" << endmsg;

	if((GaudiTool::initialize()).isFailure()){
		error() << "GenEx::GaudiTool failed to initialize" << endmsg;
		return StatusCode::FAILURE;
		} 
	
	if(!m_settings.basicSttgsCheck()) return StatusCode::FAILURE;

	///foam
	m_foam = new TFoam("FoamInstance"); //requires unique name

	///random generator
	m_randGen.initialize();

	///non-essential parameters
    m_foam->SetnCells(m_settings.foam.m_Cells);
    m_foam->SetnSampl(m_settings.foam.m_nSampl);
    m_foam->SetnBin(m_settings.foam.m_nBin);
    m_foam->SetOptRej(m_settings.foam.m_OptRej);
    m_foam->SetOptDrive(m_settings.foam.m_OptDrive);
    m_foam->SetEvPerBin(m_settings.foam.m_EvPerBin);
    m_foam->SetChat(m_settings.foam.m_Chat);	
    m_foam->SetMaxWtRej(m_settings.foam.m_MaxWtRej);


	
	///find decay products ids and set particle vectors
		//0 index is not used by the phase space algorithm
		generatorParticles.m_inParticles.first.push_back(0);
		generatorParticles.m_inParticles.second.push_back(TLorentzVector(0,0,0,0));
		generatorParticles.m_outParticles.first.push_back(0);
		generatorParticles.m_outParticles.second.push_back(TLorentzVector(0,0,0,0));	
		generatorParticles.m_outParticlesMasses.push_back(0);

		//set in protons
		generatorParticles.m_inParticles.first.push_back(2212);
		generatorParticles.m_inParticles.second.push_back(TLorentzVector(0,0,0,0));
		generatorParticles.m_inParticles.first.push_back(2212);
		generatorParticles.m_inParticles.second.push_back(TLorentzVector(0,0,0,0));
		//set out protons
		generatorParticles.m_outParticles.first.push_back(2212);
		generatorParticles.m_outParticles.second.push_back(TLorentzVector(0,0,0,0));
		generatorParticles.m_outParticles.first.push_back(2212);
		generatorParticles.m_outParticles.second.push_back(TLorentzVector(0,0,0,0));		
		generatorParticles.m_outParticlesMasses.push_back(m_particleData.GetParticle(2212)->Mass());
		generatorParticles.m_outParticlesMasses.push_back(m_particleData.GetParticle(2212)->Mass());
		
		
		if(!m_settings.process.m_decayProductsID.empty()){
			//find decay products ids in a comma separated string
			info() << "GenEx::decay products ids: " << m_settings.process.m_decayProductsID << endmsg;
			size_t pos1 = 0;
			size_t pos2 = 0;
			Int_t id;
			
			while(true){
				pos2 = m_settings.process.m_decayProductsID.find_first_of(',', pos1);

				//easiest way of handling comma left at the beginning etc.
				//as stoi throws exceptions for everything that's not a number
				try{	
					id = std::stoi(m_settings.process.m_decayProductsID.substr(pos1, pos2));
				}catch(...){ 
					error() << "GenEx::Generator something wrong with decay products ids" << endmsg;
					return StatusCode::FAILURE;
				}
				//set particles

				generatorParticles.m_outParticles.first.push_back(id);
				generatorParticles.m_outParticles.second.push_back(TLorentzVector(0,0,0,0));
				generatorParticles.m_outParticlesMasses.push_back(m_particleData.GetParticle(id)->Mass());	

				pos1 = pos2 + 1;				

				if(pos2 == std::string::npos)
					break;
			}
		}
		info() << "[DEBUG]: GenEx:::m_outParticles.first.size() " << generatorParticles.m_outParticles.first.size() << endmsg;
		if(generatorParticles.m_outParticles.first.size() == 3){ ///protons only, 0th index has to be counted, hence 3
			generatorParticles.m_decay = 0; //no decay, meaning [3] is meson
			///add meson instead
			generatorParticles.m_outParticles.first.push_back(m_settings.process.m_mesonID);
			generatorParticles.m_outParticles.second.push_back(TLorentzVector(0,0,0,0));			
			generatorParticles.m_outParticlesMasses.push_back(m_particleData.GetParticle(m_settings.process.m_mesonID)->Mass());
		}
		else if(generatorParticles.m_outParticles.first.size() == 4){ ///meson can't decay into one particle
			error() << "GenEx::Generator only one decay product found" << endmsg;
			return StatusCode::FAILURE;
		}
		else	///decay enabled, meaning no meson in m_outParticles
			generatorParticles.m_decay = 1;
	///set kinematics blob (meson) mass window

		m_settings.kinematics.internal.m_minBlobMass = m_particleData.GetParticle(m_settings.process.m_mesonID)->Mass() - m_settings.kinematics.m_mesonMassWidth;
		m_settings.kinematics.internal.m_maxBlobMass = m_particleData.GetParticle(m_settings.process.m_mesonID)->Mass() + m_settings.kinematics.m_mesonMassWidth;

	info() << "[DEBUG]: GenEx::: get the meson type: " <<m_settings.process.m_mesonID << endmsg;
	///meson type
	if(m_settings.process.m_mesonID == 443){
		m_process = new Process::JPsi(m_randGen, generatorParticles , m_settings.kinematics, m_settings.amplitude.m_abs_corr);
		info() << "[DEBUG]: GenEx:: process defined! " << endmsg;
	}
	else if(m_settings.process.m_mesonID == 333)
		m_process = new Process::Phi(m_randGen, generatorParticles , m_settings.kinematics, m_settings.amplitude.m_abs_corr);
// 	else if(m_settings.process.m_mesonID == 113)
// 		goto fornow; //m_process = new Process::Rho(m_statistics, m_randGen, m_CMEnergy, m_inParticles, m_outParticles, m_outParticlesMasses, m_meson4Vector, m_decay, m_settings. m_minPt, m_settings. m_maxPt , m_settings. m_minRapidity , m_settings. m_maxRapidity , m_settings. m_minBlobMass , m_settings. m_maxBlobMass, m_settings.m_isol, m_settings.m_abs_corr);
// 	else if(m_settings.process.m_mesonID == 223)
// 		goto fornow; //m_process = new Process::Omega(m_statistics, m_randGen, m_CMEnergy, m_inParticles, m_outParticles, m_outParticlesMasses, m_meson4Vector, m_decay, m_settings. m_minPt, m_settings. m_maxPt , m_settings. m_minRapidity , m_settings. m_maxRapidity , m_settings. m_minBlobMass , m_settings. m_maxBlobMass, m_settings.m_isol, m_settings.m_abs_corr);
// 	else if(m_settings.process.m_mesonID == 553)
// 		goto fornow; //m_process = new Process::Upsilon(m_statistics, m_randGen, m_CMEnergy, m_inParticles, m_outParticles, m_outParticlesMasses, m_meson4Vector, m_decay, m_settings. m_minPt, m_settings. m_maxPt , m_settings. m_minRapidity , m_settings. m_maxRapidity , m_settings. m_minBlobMass , m_settings. m_maxBlobMass, m_settings.m_isol, m_settings.m_abs_corr);		
	else{
// fornow:		error() << "GenEx::Desired process not found!"<< endmsg;
		error() << "GenEx::Desired process not found!"<< endmsg;
		return StatusCode::FAILURE;
	}

	info() << "[DEBUG]: GenEx:: >>> " << endmsg;

	///degrees of freedom
	m_foam->SetkDim(3 * (generatorParticles.m_outParticles.first.size() - 1) - 4);
	
	///initialize foam
	m_foam->Initialize(&m_randGen, m_process);
	

	if(m_settings.internal.m_generateNTuple){
		m_outputFile = new TFile((m_settings.internal.m_nTupleName + ".root").c_str(),"recreate");
		m_outputTree = new TTree("output", "Kinematic variables from generator; before simulation phase");

		///prepare output; 10 for both protons and meson and 10 for each decay product

		for(unsigned int i = 0; i < (30 + (generatorParticles.m_decay? 10*(generatorParticles.m_outParticles.second.size() - 3) : 0)); ++i)
			m_outputValues.push_back(0);

		///set branches
		///proton1
		m_outputTree->Branch("proton1_E", &(m_outputValues[0]),"proton1_E/D");
		m_outputTree->Branch("proton1_M", &(m_outputValues[1]),"proton1_M/D");
		m_outputTree->Branch("proton1_P", &(m_outputValues[2]),"proton1_P/D");
		m_outputTree->Branch("proton1_Px", &(m_outputValues[3]),"proton1_Px/D");
		m_outputTree->Branch("proton1_Py", &(m_outputValues[4]),"proton1_Py/D");
		m_outputTree->Branch("proton1_Pz", &(m_outputValues[5]),"proton1_Pz/D");
		m_outputTree->Branch("proton1_Pt", &(m_outputValues[6]),"proton1_Pt/D");
		m_outputTree->Branch("proton1_Phi", &(m_outputValues[7]),"proton1_Phi/D");
		m_outputTree->Branch("proton1_Rho", &(m_outputValues[8]),"proton1_Rho/D");
		m_outputTree->Branch("proton1_Eta", &(m_outputValues[9]),"proton1_Eta/D");
		///proton2
		m_outputTree->Branch("proton2_E", &(m_outputValues[10]),"proton2_E/D");
		m_outputTree->Branch("proton2_M", &(m_outputValues[11]),"proton2_M/D");
		m_outputTree->Branch("proton2_P", &(m_outputValues[12]),"proton2_P/D");
		m_outputTree->Branch("proton2_Px", &(m_outputValues[13]),"proton2_Px/D");
		m_outputTree->Branch("proton2_Py", &(m_outputValues[14]),"proton2_Py/D");
		m_outputTree->Branch("proton2_Pz", &(m_outputValues[15]),"proton2_Pz/D");
		m_outputTree->Branch("proton2_Pt", &(m_outputValues[16]),"proton2_Pt/D");
		m_outputTree->Branch("proton2_Phi", &(m_outputValues[17]),"proton2_Phi/D");
		m_outputTree->Branch("proton2_Rho", &(m_outputValues[18]),"proton2_Rho/D");
		m_outputTree->Branch("proton2_Eta", &(m_outputValues[19]),"proton2_Eta/D");
		///meson
		m_outputTree->Branch("meson_E", &(m_outputValues[20]),"meson_E/D");
		m_outputTree->Branch("meson_M", &(m_outputValues[21]),"meson_M/D");
		m_outputTree->Branch("meson_P", &(m_outputValues[22]),"meson_P/D");
		m_outputTree->Branch("meson_Px", &(m_outputValues[23]),"meson_Px/D");
		m_outputTree->Branch("meson_Py", &(m_outputValues[24]),"meson_Py/D");
		m_outputTree->Branch("meson_Pz", &(m_outputValues[25]),"meson_Pz/D");
		m_outputTree->Branch("meson_Pt", &(m_outputValues[26]),"meson_Pt/D");
		m_outputTree->Branch("meson_Phi", &(m_outputValues[27]),"meson_Phi/D");
		m_outputTree->Branch("meson_Rho", &(m_outputValues[28]),"meson_Rho/D");
		m_outputTree->Branch("meson_Eta", &(m_outputValues[29]),"meson_Eta/D");
		///decay products
		if(generatorParticles.m_decay){
			for(unsigned int i = 3; i < generatorParticles.m_outParticles.second.size(); ++i){
				int id = generatorParticles.m_outParticles.first[i];
				std::string name = std::to_string(abs(id)) + "_" + std::to_string(i-3) + (id < 0 ? "_minus" : "_plus");
					
				m_outputTree->Branch((name + "_E").c_str(), &(m_outputValues[29 + (i-3)*10 + 1]),(name + "_E/D").c_str());
				m_outputTree->Branch((name + "_M").c_str(), &(m_outputValues[29 + (i-3)*10 + 2]),(name + "_M/D").c_str());
				m_outputTree->Branch((name + "_P").c_str(), &(m_outputValues[29 + (i-3)*10 + 3]),(name + "_P/D").c_str());
				m_outputTree->Branch((name + "_Px").c_str(), &(m_outputValues[29 + (i-3)*10 + 4]),(name + "_Px/D").c_str());
				m_outputTree->Branch((name + "_Py").c_str(), &(m_outputValues[29 + (i-3)*10 + 5]),(name + "_Py/D").c_str());
				m_outputTree->Branch((name + "_Pz").c_str(), &(m_outputValues[29 + (i-3)*10 + 6]),(name + "_Pz/D").c_str());
				m_outputTree->Branch((name + "_Pt").c_str(), &(m_outputValues[29 + (i-3)*10 + 7]),(name + "_Pt/D").c_str());
				m_outputTree->Branch((name + + "_Phi").c_str(), &(m_outputValues[29 + (i-3)*10 + 8]),(name + "_Phi/D").c_str());
				m_outputTree->Branch((name + + "_Rho").c_str(), &(m_outputValues[29 + (i-3)*10 + 9]),(name + "_Rho/D").c_str());
				m_outputTree->Branch((name + + "_Eta").c_str(), &(m_outputValues[29 + (i-3)*10 + 10]),(name + "_Eta/D").c_str());

			}
		}
	}

	always() << "GenEx:: Generator initialized" << endmsg;
	return StatusCode::SUCCESS;	
}

StatusCode GenExGenerator::generateEvent(HepMC::GenEvent* event){

	always() << "GenEx::Generating event" << endmsg;

	///generate event
	m_foam->MakeEvent();

	if(m_settings.internal.m_generateNTuple){ 
	///fill helper vector
		//protons 1
		m_outputValues[0] = generatorParticles.m_outParticles.second[1].E();
		m_outputValues[1] = generatorParticles.m_outParticles.second[1].M();
		m_outputValues[2] = generatorParticles.m_outParticles.second[1].P();
		m_outputValues[3] = generatorParticles.m_outParticles.second[1].Px();
		m_outputValues[4] = generatorParticles.m_outParticles.second[1].Py();
		m_outputValues[5] = generatorParticles.m_outParticles.second[1].Pz();
		m_outputValues[6] = generatorParticles.m_outParticles.second[1].Pt();
		m_outputValues[7] = generatorParticles.m_outParticles.second[1].Phi();
		m_outputValues[8] = generatorParticles.m_outParticles.second[1].Rho();
		m_outputValues[9] = generatorParticles.m_outParticles.second[1].Eta();
		//2
		m_outputValues[10] = generatorParticles.m_outParticles.second[2].E();
		m_outputValues[11] = generatorParticles.m_outParticles.second[2].M();
		m_outputValues[12] = generatorParticles.m_outParticles.second[2].P();
		m_outputValues[13] = generatorParticles.m_outParticles.second[2].Px();
		m_outputValues[14] = generatorParticles.m_outParticles.second[2].Py();
		m_outputValues[15] = generatorParticles.m_outParticles.second[2].Pz();
		m_outputValues[16] = generatorParticles.m_outParticles.second[2].Pt();
		m_outputValues[17] = generatorParticles.m_outParticles.second[2].Phi();
		m_outputValues[18] = generatorParticles.m_outParticles.second[2].Rho();
		m_outputValues[19] = generatorParticles.m_outParticles.second[2].Eta();
		//meson
		m_outputValues[20] = generatorParticles.m_meson4Vector.E();
		m_outputValues[21] = generatorParticles.m_meson4Vector.M();
		m_outputValues[22] = generatorParticles.m_meson4Vector.P();
		m_outputValues[23] = generatorParticles.m_meson4Vector.Px();
		m_outputValues[24] = generatorParticles.m_meson4Vector.Py();
		m_outputValues[25] = generatorParticles.m_meson4Vector.Pz();
		m_outputValues[26] = generatorParticles.m_meson4Vector.Pt();
		m_outputValues[27] = generatorParticles.m_meson4Vector.Phi();
		m_outputValues[28] = generatorParticles.m_meson4Vector.Rho();
		m_outputValues[29] = generatorParticles.m_meson4Vector.Eta();
		//other particles, if there are any
		if(generatorParticles.m_decay){
			for(unsigned int i = 3; i < generatorParticles.m_outParticles.second.size(); ++i){
				m_outputValues[29 + (i-3)*10 + 1] = generatorParticles.m_outParticles.second[i].E();
				m_outputValues[29 + (i-3)*10 + 2] = generatorParticles.m_outParticles.second[i].M();
				m_outputValues[29 + (i-3)*10 + 3] = generatorParticles.m_outParticles.second[i].P();
				m_outputValues[29 + (i-3)*10 + 4] = generatorParticles.m_outParticles.second[i].Px();
				m_outputValues[29 + (i-3)*10 + 5] = generatorParticles.m_outParticles.second[i].Py();
				m_outputValues[29 + (i-3)*10 + 6] = generatorParticles.m_outParticles.second[i].Pz();
				m_outputValues[29 + (i-3)*10 + 7] = generatorParticles.m_outParticles.second[i].Pt();
				m_outputValues[29 + (i-3)*10 + 8] = generatorParticles.m_outParticles.second[i].Phi();
				m_outputValues[29 + (i-3)*10 + 9] = generatorParticles.m_outParticles.second[i].Rho();
				m_outputValues[29 + (i-3)*10 + 10] = generatorParticles.m_outParticles.second[i].Eta();				
			}
		}
		m_outputTree->Fill();
	}

	///populate event
	
	//helper lambda function converting particle data to new instance of HepMC::GenParticle class
	//GeV converted to MeV before realising to event loop
	auto TLVtoGP = [](TLorentzVector const& tlv, Int_t const& id, Int_t const& status)->HepMC::GenParticle*{
		return new HepMC::GenParticle(HepMC::FourVector(tlv.Px()*1000, tlv.Py()*1000, tlv.Pz()*1000, tlv.E()*1000), id, status);
	};

		//production vertex
		HepMC::GenVertex* vert = new HepMC::GenVertex;
		event->add_vertex(vert);
		event->set_signal_process_vertex(vert);
		//add in protons, out protons and meson

		HepMC::GenParticle* beam1 = TLVtoGP(generatorParticles.m_inParticles.second[1], generatorParticles.m_inParticles.first[1], 4);
		HepMC::GenParticle* beam2 = TLVtoGP(generatorParticles.m_inParticles.second[2], generatorParticles.m_inParticles.first[2], 4);
		HepMC::GenParticle* meson = TLVtoGP(generatorParticles.m_meson4Vector, m_settings.process.m_mesonID, generatorParticles.m_decay ? 2 : 1);

		//vert->add_particle_in(beam1);
		//vert->add_particle_in(beam2);
		event->set_beam_particles(beam1, beam2);
		
		vert->add_particle_out(TLVtoGP(generatorParticles.m_outParticles.second[1], generatorParticles.m_outParticles.first[1], 1));//p+ out
		vert->add_particle_out(TLVtoGP(generatorParticles.m_outParticles.second[2], generatorParticles.m_outParticles.first[2], 1));//p+ out		
		vert->add_particle_out(meson);

		///if meson was decayed, add additional vertex
		if(generatorParticles.m_decay){
			HepMC::GenVertex* vert2 = new HepMC::GenVertex;
			event->add_vertex(vert2);
			//add incoming meson
			vert2->add_particle_in(meson);
			///add decay products; 0,1-p,2-p,3 - decayed; 3 is meson only if decay == 0
			for(unsigned int i = 3; i < generatorParticles.m_outParticles.second.size(); ++i)
				vert2->add_particle_out(TLVtoGP(generatorParticles.m_outParticles.second[i], generatorParticles.m_outParticles.first[i], 1));
		}


	return StatusCode::SUCCESS;	
}

StatusCode GenExGenerator::finalize(){
	
	always() << "GenEx::Finalizing generator" << endmsg;

	Double_t IntNorm, Errel; //dummy variables needed to finalize foam
	m_foam->Finalize(IntNorm, Errel);
	m_randGen.finalize();

	if(m_foam != nullptr) {delete m_foam; m_foam = nullptr;}
	if(m_process != nullptr) {delete m_process; m_process = nullptr;}

	if(m_settings.internal.m_generateNTuple){ 
		m_outputFile->cd();
		m_outputTree->Write();
		m_outputFile->Close();
	}

	return GaudiTool::finalize();
}

