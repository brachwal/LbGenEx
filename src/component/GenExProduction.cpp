#include "GenExProduction.h"

// Declaration of the tool factory.
DECLARE_COMPONENT(GenExProduction)


GenExProduction::GenExProduction(const std::string& type, const std::string& name, const IInterface* parent)
    :   GaudiTool(type, name, parent){

    info() << "Instantiating GenExProduction" << endmsg;

    declareInterface<IProductionTool>(this);

    declareSettings();

	if(m_beamtool == nullptr)
		m_beamtool = tool<IBeamTool>(m_settings.internal.m_beamtoolName, this);
}

GenExProduction::~GenExProduction(){}

void GenExProduction::declareSettings(){

	//beam tool 
    declareProperty("internal_beamToolName", m_settings.internal.m_beamtoolName = "CollidingBeams");
	//generation m_settings
	declareProperty("process_mesonId", m_settings.process.m_mesonID = -9999);
	declareProperty("process_decayProductsIds", m_settings.process.m_decayProductsID = "-13,13");

	declareProperty("kinematics_solutionType", m_settings.kinematics.m_isol = 0);
	declareProperty("amplitudes_absorptionCorrections", m_settings.amplitude.m_abs_corr = 0);
	
	//foam settings///descriptions copied from foam documentation
    declareProperty("foam_numberOfCells", m_settings.foam.m_Cells = 10000);
    declareProperty("foam_numberOfSamples", m_settings.foam.m_nSampl = 10000);
    declareProperty("foam_numberOfBins", m_settings.foam.m_nBin = 8);
    declareProperty("foam_optRej", m_settings.foam.m_OptRej = 1);
    declareProperty("foam_optDrive", m_settings.foam.m_OptDrive = 2);
    declareProperty("foam_eventsPerBin", m_settings.foam.m_EvPerBin = 25);
    declareProperty("foam_chatLevel", m_settings.foam.m_Chat = 1);
    declareProperty("foam_maxWtRej", m_settings.foam.m_MaxWtRej = 1.1);
	
	//phase-space cuts
	declareProperty("kinematics_mesonMassWidth", m_settings.kinematics.m_mesonMassWidth = 0.00001);
	declareProperty("kinematics_minPt", m_settings.kinematics.cuts.m_minPt);
	declareProperty("kinematics_maxPt", m_settings.kinematics.cuts.m_maxPt);
	declareProperty("kinematics_minRapidity", m_settings.kinematics.cuts.m_minRapidity);
	declareProperty("kinematics_maxRapidity", m_settings.kinematics.cuts.m_maxRapidity);
	

	declareProperty("internal_generateNTuple", m_settings.internal.m_generateNTuple = 0);
	declareProperty("internal_nTupleName", m_settings.internal.m_nTupleName = "output");
	
}

StatusCode GenExProduction::initialize(){
	return GaudiTool::initialize();
}

StatusCode GenExProduction::initializeGenerator(){
	
	Gaudi::XYZVector beam1, beam2;

	m_beamtool->getMeanBeams(beam1, beam2);
	
	Double_t e1 = sqrt(beam1.Mag2());
	Double_t e2 = sqrt(beam2.Mag2());
	Double_t ecm = sqrt((e1 + e2)*(e1 + e2) - (beam1 + beam2).Mag2())/Gaudi::Units::GeV;
	//if (ecm > 1000) ecm /= 1000;

	info() << "LbGenex CMSEnergy: " << ecm << "  GeV" << endmsg;
	
	m_GenEx = new GenEx::GenExGenerator("","GenExGenerator", this, m_settings, ecm);

	

	return m_GenEx->initialize(); //StatusCode::SUCCESS;
}

StatusCode GenExProduction::generateEvent(HepMC::GenEvent* event, LHCb::GenCollision* /*collision*/){
	return m_GenEx->generateEvent(event);
}

StatusCode GenExProduction::finalize(){
	m_GenEx->finalize();

	if(m_GenEx != nullptr) {delete m_GenEx; m_GenEx = nullptr;}

	return GaudiTool::finalize();
}

///dummy functions overriding pure virtual members of IProductionTool

void GenExProduction::setStable(const LHCb::ParticleProperty */*thePP*/) {}

void GenExProduction::updateParticleProperties( const LHCb::ParticleProperty */*thePP*/) {}

void GenExProduction::savePartonEvent(HepMC::GenEvent */*theEvent*/) {}

void GenExProduction::retrievePartonEvent(HepMC::GenEvent */*theEvent*/) {}

StatusCode GenExProduction::hadronize(HepMC::GenEvent */*theEvent*/, LHCb::GenCollision */*theCollision*/)
		{return StatusCode::SUCCESS;}

void GenExProduction::printRunningConditions() {}

bool GenExProduction::isSpecialParticle(const LHCb::ParticleProperty * /*thePP*/) const 
		{return false;}

StatusCode GenExProduction::setupForcedFragmentation(const int /*thePdgId*/) 
	{return StatusCode::SUCCESS;}

void GenExProduction::turnOnFragmentation() {}

void GenExProduction::turnOffFragmentation() {}
