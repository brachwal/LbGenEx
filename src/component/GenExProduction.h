#ifndef GENEXPRODUCTION_H
#define GENEXPRODUCTION_H
#include "GenExGenerator.h"
#include "GenExSettings.h"

//standard libraries
#include <string>
#include <utility>
// Gaudi.
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/System.h"
// #include "GaudiKernel/DeclareFactoryEntries.h"
#include "Kernel/ParticleProperty.h"
#include "Kernel/IParticlePropertySvc.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"

// Event.
#include "Event/GenCollision.h"

// Generators .
#include "Generators/StringParse.h"
#include "Generators/IProductionTool.h"
#include "Generators/IBeamTool.h"

// HepMC.
#include "Event/HepMCEvent.h"


/**	@class GenExProduction GenExProduction.h src/GenExProduction.h
*
*	LHCb interface of the GenEx generator.
*
*	@author Mateusz Goncerz
*	@date 2017-07-15
*
*/
class GenExProduction : public GaudiTool, virtual public IProductionTool{


public:
	
	///default constructor disabled
	GenExProduction() = delete;
	
	/**
	*	Standard GaudiTool constructor.
	*
	*	Additionally it initializes GenEx::Settings class,
	*	declares the settings and initializes beamtool.
	*
	*/
    GenExProduction(const std::string& type, const std::string & name, const IInterface* parent);

	///default destructor
    virtual ~GenExProduction();

	virtual StatusCode initialize();
	
	/**
	*	Creates and initializes an instance of GenEx::GenExGenerator,
	*	passing the reference to the beam 3vectors pair.
	*
	*/
	virtual StatusCode initializeGenerator();

	/**
	*	Gets the beam 3vectors from beamtool
	*	and calls GenEx generateEvent() method
	*
	*/
  	virtual StatusCode generateEvent( HepMC::GenEvent * , LHCb::GenCollision *);

	///Finalizes the generator and performs a cleanup
    virtual StatusCode finalize();			


private:

	///sets the settings with declareProperty
	void declareSettings();

	///generator instance
	GenEx::GenExGenerator* m_GenEx = nullptr;

	///settings instance, shared with generator
	GenEx::GenExSettings m_settings;

	///beamtool instance
	IBeamTool* m_beamtool = nullptr;

	///beam 3vectors, shared with generator
	std::pair<HepMC::GenParticle*, HepMC::GenParticle*> beam; 

public: ///dummy functions overriding pure virtual members of IProductionTool

	virtual void setStable(const LHCb::ParticleProperty */*thePP*/);

	virtual void updateParticleProperties( const LHCb::ParticleProperty */*thePP*/);

	virtual void savePartonEvent(HepMC::GenEvent */*theEvent*/);

	virtual void retrievePartonEvent(HepMC::GenEvent */*theEvent*/);

	virtual StatusCode hadronize(HepMC::GenEvent * /*theEvent*/, LHCb::GenCollision * /*theCollision*/);

	virtual void printRunningConditions();

	virtual bool isSpecialParticle(const LHCb::ParticleProperty * /*thePP*/) const;

	virtual StatusCode setupForcedFragmentation(const int /*thePdgId*/);

	virtual void turnOnFragmentation();

	virtual void turnOffFragmentation();

};


#endif
