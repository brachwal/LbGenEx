from Configurables import *

# Configure Gauss.
OutputStream('GaussTape').Output = ("DATAFILE='PFN:gauss.xgen' TYP='POOL_ROOTTREE' OPT='RECREATE'")
#OutputStream('GaussTape').Output = ("DATAFILE='PFN:gauss.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'")
LHCbApp().EvtMax = 10
LHCbApp().DDDBtag = "dddb-20170721-2"
LHCbApp().CondDBtag = "sim-20160321-2-vc-md100"
Gauss().Phases = ["Generator", "GenToMCTree"]
#Gauss().Phases = ["Generator", "Simulation"]
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber = 2
