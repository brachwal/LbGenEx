#!/usr/bin/env bash

/afs/cern.ch/user/b/brachwal/cmtuser/GaussLbGenEx/build.x86_64-centos7-gcc9-opt/run gaudirun.py '$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2016-nu1.6.py' '$APPCONFIGOPTS/Gauss/DataType-2016.py' '$APPCONFIGOPTS/Gauss/RICHRandomHits.py' '$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py' '$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py' '$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py' '../options/49142010.py' 'example.py'
