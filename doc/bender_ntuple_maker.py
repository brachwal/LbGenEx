# to be ran with:
# $ lb-run Bender/v33r3 python zbb_bender_ntuple_maker.py name_of_your_file_gauss.xgen

from Bender.MainMC import *
import sys

class FillTuple(AlgoMC):
  ## Define generic function to add information to tuple (remove information not needed):
  def save_event(self, tup, particle, name):
    tup.column_float(name+'_M',              MCM(particle))    # MeV
    tup.column_float(name+'_E',              MCE(particle))    # MeV
    tup.column_float(name+'_PX',             MCPX(particle))   # MeV
    tup.column_float(name+'_PY',             MCPY(particle))   # MeV
    tup.column_float(name+'_PZ',             MCPZ(particle))   # MeV
    tup.column_float(name+'_PT',             MCPT(particle))   # MeV
    tup.column_float(name+'_ETA',            MCETA(particle))
    tup.column_float(name+'_ID',             MCID(particle)) 
    tup.column_float(name+'_ORIGINVERTEX_X', MCVFASPF(MCVX)(particle)) 
    tup.column_float(name+'_ORIGINVERTEX_Y', MCVFASPF(MCVY)(particle)) 
    tup.column_float(name+'_ORIGINVERTEX_Z', MCVFASPF(MCVZ)(particle)) 
    tup.column_float(name+'_LIFETIME_CTAU',  MCCTAU(particle))
    return tup
  ### Main body:
  def analyse(self):
    particles = self.mcselect('ALL',  MCALL)
    if particles.empty(): return self.Warning('No input particles', SUCCESS)
    print 'Found  %d particles ' % len ( particles )

    tup = self.nTuple('MC_TUPLE')
    #for particle in particles:  
#	particleMotherId = int(str(int(MCABSID(particle)))[:1])
#	if int(MCID(particle))==5:
#	    tup.column_float('b_M',MCM(particle))
#	    tup.column_float('b_PT',MCPT(particle))
#	if int(MCID(particle))==-5:
#            tup.column_float('anti_b_M',MCM(particle))
#            tup.column_float('anti_b_PT',MCPT(particle))

    for particle in particles:  
        if int(MCABSID(particle))==443: # look for Jpsi 
            tup = self.save_event(tup, particle, 'Jpsi')
	    print 'Jpsi mass %f' % MCM(particle)
            for muons in particle.children(): # look for the muons from the Z0
              if int(MCID(muons))==13: # mu-
                tup = self.save_event(tup, muons, 'mu_minus')
              if int(MCID(muons))==-13: # mu+
                tup = self.save_event(tup, muons, 'mu_plus')
            tup.write()
    ##################################################      
    return SUCCESS

def configure(inputdata, catalogs = [], castor = False):
  from Configurables import DaVinci
  dv = DaVinci(DataType   = '2016',
               TupleFile  = 'MC_TUPLE.root',
               Simulation = True,
               Lumi       = False,
               DDDBtag    = "dddb-20170721-2", # should be updated
               CondDBtag  = "sim-20160321-2-vc-md100" ) # should be updated

  alg_name = 'MC_Zbb_TUPLE'
  dv.UserAlgorithms += [ alg_name ]
  setData(inputdata, catalogs, castor)
  gaudi = appMgr()
  alg = FillTuple(alg_name)
  return SUCCESS

from Gaudi.Configuration import appendPostConfigAction
def doIt():
    """
    specific post-config action for (x)GEN-files 
    """
    extension = "xgen"
    ext = extension.upper()

    from Configurables import DataOnDemandSvc
    dod  = DataOnDemandSvc ()
    from copy import deepcopy 
    algs = deepcopy ( dod.AlgMap ) 
    bad  = set() 
    for key in algs :
        if     0 <= key.find ( 'Rec'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'Raw'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'DAQ'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'Trigger' )                  : bad.add ( key )
        elif   0 <= key.find ( 'Phys'    )                  : bad.add ( key )
        elif   0 <= key.find ( 'Prev/'   )                  : bad.add ( key )
        elif   0 <= key.find ( 'Next/'   )                  : bad.add ( key )
        elif   0 <= key.find ( '/MC/'    ) and 'GEN' == ext : bad.add ( key )
        
    for b in bad :
        del algs[b]
            
    dod.AlgMap = algs
    
    from Configurables import EventClockSvc, CondDB 
    EventClockSvc ( EventTimeDecoder = "FakeEventTime" )
    CondDB  ( IgnoreHeartBeat = True )
    
appendPostConfigAction( doIt )


if __name__ == '__main__' :
  xgenFile = sys.argv[1]
  print 'Processing xgen file: {0}'.format(xgenFile)
  inputdata = [str(xgenFile)]
  configure(inputdata, castor = False)
  run(-1)

